""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                   Vundle                                   "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" install Vundle
" git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

set nocompatible               " be iMproved
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" Many plugins work with python >= 2.6
" Check if vim is compiled with python and you have python installed
" :echo has("python")

" Vim plugin management
Bundle 'gmarik/vundle'

" Runtime files
Bundle 'plasticboy/vim-markdown'
Bundle 'tpope/vim-git'
Bundle 'Keithbsmiley/tmux.vim'
Bundle 'othree/html5.vim'
Bundle 'pangloss/vim-javascript'
Bundle 'vim-scripts/django.vim'
Bundle 'hail2u/vim-css3-syntax'
Bundle 'othree/javascript-libraries-syntax.vim'
Bundle 'cespare/vim-toml'

" Text objects
Bundle 'kana/vim-textobj-user'
Bundle 'kana/vim-textobj-function'
Bundle 'lucapette/vim-textobj-underscore'
Bundle 'michaeljsmith/vim-indent-object'
Bundle 'wellle/targets.vim'
Bundle 'kana/vim-textobj-datetime'
Bundle 'kana/vim-textobj-entire'
Bundle 'coderifous/textobj-word-column.vim'

" Easily search for, substitute, and abbreviate multiple variants of a word
Bundle 'tpope/vim-abolish'

" Ack for vim (grep for source code)
" Need Ack (ubuntu: apt-get install ack-grep)
Bundle 'mileszs/ack.vim'

" Lean & mean status/tabline
" Need patched font, see
" https://powerline.readthedocs.org/en/latest/installation/linux.html#font-installation
" Bundle 'bling/vim-airline'
" Airline for tmux
"Bundle 'edkolev/tmuxline.vim'
" Airline for bash
"Bundle 'edkolev/promptline.vim'
"
" Manipulating and moving between function arguments
Bundle 'PeterRincker/vim-argumentative'

" Ctags and cscope/global management
Bundle 'carylorrk/vim-autotags-gtags'

" Auto closing tool
"Bundle 'Raimondi/delimitMate'
Bundle 'jiangmiao/auto-pairs'

" calendar vimscript
" Bundle 'mattn/calendar-vim'

" color ansi sequences, hex code and color name
" Bundle 'chrisbra/Colorizer'
"Bundle 'sjl/AnsiEsc.vim'

" Clang Format
Bundle 'rhysd/vim-clang-format'

" Color Scheme
Bundle 'chriskempson/vim-tomorrow-theme'
"Bundle 'chriskempson/base16-vim'

" Fuzzy File Finder
Bundle 'kien/ctrlp.vim'

" Ex command history reverse-i-search
Bundle 'goldfeld/ctrlr.vim'

" Asynchronous build and test dispatcher
Bundle 'tpope/vim-dispatch'

" Generate Doxygen documentation for C/C++, Python
" Bundle 'vim-scripts/DoxygenToolkit.vim'

" A Vim alignment plugin
Bundle 'junegunn/vim-easy-align'

" Simpler way to move
Bundle 'Lokaltog/vim-easymotion'
" Bundle 'haya14busa/vim-easyoperator-phrase'
" Bundle 'haya14busa/vim-easyoperator-line'

" Converts all compiler errors into signs
Bundle 'vim-scripts/errormarker.vim'

" Write HTML in a CSS-like syntax
"Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
Bundle 'mattn/emmet-vim'

" Wisely \"end\" endfunction/endif/more in vim script
Bundle 'tpope/vim-endwise'

" Helpers for UNIX
Bundle 'tpope/vim-eunuch'

" Enabling opening a file in a given line
" Bundle 'bogado/file-line'

" Git for VIM
" Bundle 'tpope/vim-fugitive'
" Shows a git diff in the gutter (sign column)
" Bundle 'airblade/vim-gitgutter'
" Search Github and clone repos
" Bundle 'gmarik/github-search.vim'

" Go development plugin for Vim
Bundle 'fatih/vim-go'

" Global search and replace
" Bundle 'skwp/greplace.vim'

" Show undo tree
Bundle 'sjl/gundo.vim'

" Visually displaying indent levels in code
" Bundle 'nathanaelkane/vim-indent-guides'

" Instant Markdown previews from VIM
" Need some prerequisites.
" gem install pygments.rb
" gem install redcarpet -v 2.3.0
" npm config set registry http://registry.npmjs.org/
" npm -g install instant-markdown-d
" ndg-utils
"Bundle 'suan/vim-instant-markdown'

" Lightweight Toolbox for LaTeX
" Bundle 'LaTeX-Box-Team/LaTeX-Box.git'

" Perform diffs on blocks of code
" Bundle 'AndrewRadev/linediff.vim'

" Local vimrc (.lvimrc)
" Bundle 'vim-scripts/localvimrc'

" Sublime Text-like multiple selection
" Bundle 'terryma/vim-multiple-cursors'

" Filesystem explorer
Bundle 'vim-scripts/The-NERD-tree'
Bundle 'jistr/vim-nerdtree-tabs'

" Fast comment
Bundle 'scrooloose/nerdcommenter'

" A Narrow Region Plugin likes Emacs Narrow Region
" Bundle 'chrisbra/NrrwRgn'

" Improved PHP omnicompletion
Bundle 'shawncplus/phpcomplete.vim'

" Vim python-mode. PyLint, Rope, Pydoc, breakpoints from box.
Bundle 'klen/python-mode'

" Run commands quickly
Bundle 'thinca/vim-quickrun'

" Repeat previous action
Bundle 'tpope/vim-repeat'

" Session management
Bundle 'xolox/vim-session'
Bundle 'xolox/vim-misc'

" Vim filetype detection by the shebang
Bundle 'vitalk/vim-shebang'

" Use CTRL-A/CTRL-X to increment dates, times, and more
" Bundle 'tpope/vim-speeddating'

" Surround
Bundle 'tpope/vim-surround'

" A fancy start screen
"Bundle 'mhinz/vim-startify'

" Rust development enviroment
Plugin 'rust-lang/rust.vim'

" Syntax checking
Bundle 'scrooloose/syntastic'

" Taglist replacement
Bundle 'vim-scripts/Tagbar'

" Eclipse like task list
" Bundle 'vim-scripts/TaskList.vim'

" Pairs of handy bracket mappings
Bundle 'tpope/vim-unimpaired'

" Maintains a history of previous yanks, changes and deletes
Bundle 'vim-scripts/YankRing.vim'

" Autocomplete engine
" needs some prerequisites.
" sudo apt-get install build-essential cmake python-dev
" cd ~/.vim/bundle/YouCompleteMe
" ./install.sh --clang-completer
" if jedi is not founded, run 'git submodule update --init --recursive' at YCM
Bundle 'Valloric/YouCompleteMe'

" Autocomplete
" Bundle 'Shougo/neocomplcache.vim'

" Snippet managemet (Code template)
Bundle 'SirVer/ultisnips'
Bundle 'honza/vim-snippets'
Bundle 'matthewsimo/angular-vim-snippets'
Bundle 'CaryLorrk/vim-snippets-carylorrk'


""""""""""""""""""
"  Undocumented  "
""""""""""""""""""

" Text outlining and task management
"Bundle 'jceb/vim-orgmode'
" Execute URLs, footnotes, open emails, organize ideas
"Bundle 'vim-scripts/utl.vim'

filetype plugin indent on     " required!

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Ultisnips setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:UltiSnipsExpandTrigger="<LocalLeader><TAB>"
let g:UltiSnipsListSnippets="<localLeader>l"

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              NERDTree setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <Leader>e :NERDTreeTabsToggle <CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Tagbar setting                               "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <Leader>l :TagbarToggle   <CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Gundo setting                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <Leader>u :GundoToggle    <CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Easymotion setting                             "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:EasyMotion_smartcase        = 1
let g:EasyMotion_use_upper        = 1
let g:EasyMotion_keys             = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ;'
let g:EasyMotion_enter_jump_first = 1
let g:EasyMotion_space_jump_first = 1
map  f <Plug>(easymotion-bd-fl)
map  F <Plug>(easymotion-bd-f)
"map  / <Plug>(easymotion-sn)
"omap / <Plug>(easymotion-tn)
"map  n <Plug>(easymotion-next)
"map  N <Plug>(easymotion-prev)
omap <Leader>p  <Plug>(easyoperator-phrase-select)
xmap <Leader>p  <Plug>(easyoperator-phrase-select)
nmap d<Leader>p <Plug>(easyoperator-phrase-delete)
nmap y<Leader>p <Plug>(easyoperator-phrase-yank)
omap <Leader>l  <Plug>(easyoperator-line-select)
xmap <Leader>l  <Plug>(easyoperator-line-select)
nmap d<Leader>l <Plug>(easyoperator-line-delete)
nmap y<Leader>l <Plug>(easyoperator-line-yank)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               CtrlP setting                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:ctrlp_map               = '<Leader>f'
let g:ctrlp_cmd               = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_custom_ignore     = '\v[\/]\.(git|hg|svn|pyc)$'
let g:ctrlp_regexp            = 0
let g:ctrlp_extensions        = [ 'tag', 'buffertag' ]
let g:ctrlp_switch_buffer     = 0
let g:ctrlp_match_window      = 'order:ttb, max:20'
let g:ctrlp_user_command      = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
nmap <Leader>b  :CtrlPBuffer    <CR>
nmap <leader>t  :CtrlPBufTag    <CR>
nmap <Leader>T  :CtrlPBufTagAll <CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              YankRing setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:yankring_replace_n_pkey = '<Leader>p'
let g:yankring_replace_n_nkey = '<Leader>n'
let g:yankring_history_dir    = $HOME."/.vim"
nnoremap <silent> <Leader>yr :YRShow <CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Autotags setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:autotagsdir     = $HOME . "/.vim/autotags/byhash"
let g:autotags_global = $HOME . "/.vim/autotags/global_tags"
let g:autotags_no_global = 1

if !isdirectory(g:autotagsdir)
    call mkdir(g:autotagsdir, "p")
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Session setting                               "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
fun! PathToFilename(val)
    return substitute(a:val, "/", "%", "g")
endfun

nmap <Leader>ss :execute "SaveSession" PathToFilename(getcwd())   <CR>
nmap <Leader>so :execute "OpenSession" PathToFilename(getcwd())   <CR>
nmap <Leader>sc :execute "CloseSession"                           <CR>
nmap <Leader>sd :execute "DeleteSession" PathToFilename(getcwd()) <CR>
let g:session_default_overwrite = 1
let g:session_autoload = 'no'
let g:session_autosave = 'no'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Dispatch setting                             "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Debug after make
nmap <Leader>q :Copen <CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           YouCompleteMe setting                            "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <Leader>yy :YcmDiags                                   <CR>
nmap <Leader>yf :YcmForceCompileAndDiagnostics              <CR>
nmap <Leader>yd :YcmCompleter GoToDefinitionElseDeclaration <CR>
nmap <Leader>yc :YcmCompleter GoToDeclaration               <CR>
let g:ycm_complete_in_comments                      = 1
let g:ycm_collect_identifiers_from_tags_files       = 1
let g:ycm_min_num_of_chars_for_completion           = 1
let g:ycm_seed_identifiers_with_syntax              = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              TaskList setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <Leader>v <Plug>TaskList

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                Utl setting                                 "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:utl_cfg_hdl_mt_generic      = "silent !xdg-open %p"
let g:utl_cfg_hdl_scm_http_system = "silent !xdg-open %u"
let g:utl_cfg_hdl_mt_text_html    = 'VIM'
let g:utl_cfg_hdl_scm_http        = g:utl_cfg_hdl_scm_http_system
nmap <Leader><Leader>uu  :Utl                                                      <CR>
nmap <Leader><Leader>uhs :let g:utl_cfg_hdl_scm_http=g:utl_cfg_hdl_scm_http_system <CR>
nmap <Leader><Leader>uhw :let g:utl_cfg_hdl_scm_http=g:utl_cfg_hdl_scm_http__wget  <CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Calendar setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:calendar_keys = {
            \ 'do_action' : 'o',
            \ 'goto_next_month' : ']m',
            \ 'goto_prev_month' : '[m',
            \ 'goto_next_year' : ']y',
            \ 'goto_prev_year'  : '[y' }
let g:calendar_diary=$HOME.'/.vim/diary'
if !isdirectory(g:calendar_diary)
    call mkdir(g:calendar_diary, "p")
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Airline setting                               "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:airline_powerline_fonts            = 1
let g:airline#extensions#tabline#enabled = 1
let g:tmuxline_theme                     = 'iceberg'
let g:promptline_theme                   = 'airline_insert'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             EasyAlign setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
vmap <Enter>   <Plug>(EasyAlign)
map <Leader>a :EasyAlign
 let g:easy_align_delimiters = {
\ '>': { 'pattern': '>>\|=>\|>' },
\ '/': { 'pattern': '//\+\|/\*\|\*/', 'ignore_groups': ['String'] },
\ '#': { 'pattern': '#\+', 'ignore_groups': ['String'], 'delimiter_align': 'l' },
\ ']': {
\     'pattern':       '[[\]]',
\     'left_margin':   0,
\     'right_margin':  0,
\     'stick_to_left': 0
\   },
\ ')': {
\     'pattern':       '[()]',
\     'left_margin':   0,
\     'right_margin':  0,
\     'stick_to_left': 0
\   },
\ 'd': {
\     'pattern': ' \(\S\+\s*[;=]\)\@=',
\     'left_margin': 0,
\     'right_margin': 0
\   }
\ }

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                            Colorscheme setting                             "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
colorscheme Tomorrow-Night
set background=dark
set cursorline
hi CursorLine term=bold cterm=bold

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Emmet setting                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:user_emmet_expandabbr_key = '<C-E>'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Colorizer setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <Leader><Leader>c :ColorToggle <CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Syntastic setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:syntastic_error_symbol   = '✗'
let g:syntastic_warning_symbol = '⚠'
let g:syntastic_mode_map = { "mode": "active",
            \ "active_filetypes": [],
            \ "passive_filetypes": ["go"] }
let g:syntastic_javascript_checkers = ['jshint']


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Greplace setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set grepprg=ack-grep
let g:grep_cmd_opts = '--noheading'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Auto Pair                                  "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:AutoPairs                   = {'(':')', '[':']', '{':'}',"'":"'",'"':'"', '`':'`'}
let g:AutoPairsShortcutToggle     = '<C-P>a'
let g:AutoPairsShortcutJump       = '<C-P>n'
let g:AutoPairsFlyMode            = 0
let g:AutoPairsShortcutBackInsert = '<C-P>b'
let g:AutoPairsShortcutFastWrap   = '<C-L>'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Indent Guides                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:indent_guides_default_mapping = 0
nmap <silent> <Leader><Leader>i <Plug>IndentGuidesToggle

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               NERDCommenter                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <Leader>caa <plug>NERDCommenterAltDelims

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                            Python Mode Setting                             "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:pymode_rope = 0 "use jedi instead
au FileType py let g:pymode_breakpoint_bind = '<leader>B'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                             Localvimrc Setting                             "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:localvimrc_sandbox = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                              Markdown Setting                              "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufRead,BufNewFile *.md set filetype=markdown

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Django Setting                               "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"autocmd FILETYPE html set filetype=htmldjango.html
"autocmd FILETYPE python set filetype=python.django

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Latex Setting                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:tex_flavor='latex'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Eclim Setting                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
au FileType java nnoremap <silent><buffer><leader>ji :JavaImport<cr>
au FileType java nnoremap <silent><buffer><leader>jk :JavaDocSearch -x declarations<cr>
au FileType java nnoremap <silent><buffer><cr> :JavaSearchContext<cr>
let g:EclimCompletionMethod = 'omnifunc'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Go Setting                                 "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
au FileType go nmap <Leader>gs <Plug>(go-implements)
au FileType go nmap <Leader>gi <Plug>(go-info)
au FileType go nmap <Leader>gdo <Plug>(go-doc)
au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
au FileType go nmap <Leader>gb <Plug>(go-doc-browser)
au FileType go nmap <Leader>gr <Plug>(go-run)
au FileType go nmap <Leader>gb <Plug>(go-build)
au FileType go nmap <Leader>gt <Plug>(go-test)
au FileType go nmap <Leader>gc <Plug>(go-coverage)
au FileType go nmap <Leader>gdd <Plug>(go-def)
au FileType go nmap <Leader>gds <Plug>(go-def-split)
au FileType go nmap <Leader>gdv <Plug>(go-def-vertical)
au FileType go nmap <Leader>gdt <Plug>(go-def-tab)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                Clang Format                                "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:clang_format#style_options = {
            \ "AccessModifierOffset" : -4,
            \ "AllowShortIfStatementsOnASingleLine" : "true",
            \ "AlwaysBreakTemplateDeclarations" : "true",
            \ "Standard" : "C++11"}

" map to <Leader>cf in C++ code
autocmd FileType c,cpp,objc nnoremap <buffer><Leader>cf :<C-u>ClangFormat<CR>
autocmd FileType c,cpp,objc vnoremap <buffer><Leader>cf :ClangFormat<CR>
